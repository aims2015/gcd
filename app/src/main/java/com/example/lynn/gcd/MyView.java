package com.example.lynn.gcd;

import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import static com.example.lynn.gcd.MainActivity.*;

/**
 * Created by lynn on 6/22/2015.
 */

public class MyView extends LinearLayout {
    public MyView(Context context) {
        super(context);

        Button button = new Button(context);

        button.setText("Press Me");

        button.setOnClickListener(listener);

        addView(button);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100,100);

        input1 = new EditText(context);

        input1.setLayoutParams(layoutParams);

        input2 = new EditText(context);

        input2.setLayoutParams(layoutParams);

        addView(input1);
        addView(input2);
    }
}
