package com.example.lynn.gcd;

import android.view.View;
import android.widget.Toast;

import static com.example.lynn.gcd.MainActivity.*;

/**
 * Created by lynn on 6/22/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        int first = 0;

        int second = 0;

        boolean error = false;

        try {
            first = Integer.parseInt(input1.getText().toString());
        } catch (NumberFormatException nfe) {
            Toast.makeText(v.getContext(),"The first number is not an integer",Toast.LENGTH_LONG).show();

            error = true;
        }

        try {
            second = Integer.parseInt(input2.getText().toString());
        } catch (NumberFormatException nfe) {
            Toast.makeText(v.getContext(),"The second number is not an integer",Toast.LENGTH_LONG).show();

            error = true;
        }

        if (!error) {
            String message = "The greatest common divisor of " + first + " and " + second + " is ";

            int remainder = 1;

            while (remainder > 0) {
                remainder = first % second;
                first = second;
                second = remainder;
            }

            message += first + ".";

            Toast.makeText(v.getContext(), message, Toast.LENGTH_LONG).show();
        }
    }

}
